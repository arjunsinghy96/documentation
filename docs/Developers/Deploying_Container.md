# Deploying GitMate

!!! note
    Make sure you have [docker](https://www.docker.com/get-docker) installed
    before you proceed any further.

### 1. Getting required images

Our setup requires four containers to be run in parallel. A `postgres`
container, one for `rabbitmq`, a couple more for GitMate backend and celery
workers. These are nonetheless captured by `docker-compose`. We also require
the following two images which work with the code analysis plugin. They are
hosted on GitLab container registry. Follow these steps to pull them.

- [coala-incremental-results : latest](https://gitlab.com/gitmate/open-source/coala-incremental-results/container_registry)
- [result-bouncer : latest](https://gitlab.com/gitmate/open-source/result-bouncer/container_registry)


Login GitLab container registry with your GitLab credentials.

```bash
docker login registry.gitlab.com
```

To pull the images, run:

```bash
docker pull registry.gitlab.com/gitmate/open-source/coala-incremental-results
docker pull registry.gitlab.com/gitmate/open-source/result-bouncer
```

### 2. Using docker-compose

All our services are spun together with `docker-compose` sevices. You can view
and modify the configuration here at [docker-compose.yml.](https://gitlab.com/gitmate/open-source/gitmate-2/blob/master/docker-compose.yml)

The [required environment variables](Development_Setup/#configuring-environment-variables) can be setup at [docker/environment.](https://gitlab.com/gitmate/open-source/gitmate-2/blob/master/docker/environment)

Build the images, by running the command inside the clone gitmate-2
directory.

```bash
docker-compose build
```

To start the containers, run:

```bash
docker-compose start
```

You can now attach to any containers to get their debug logs. for e.g. to check
worker logs, run:

```bash
docker attach gitmate2_worker_1
```

Or, you could just upstart and build at the same time with continuous logs.

```bash
docker-compose up
```

!!! note
    The step below is only helpful if you want to run locally and test gitmate
    containers.

### 3. Expose your local server to the internet

We recommend using [localtunnel](https://localtunnel.me) to get a proper
deterministic URL to test GitMate with.

To expose your local server, run:
```bash
lt -s <domainname> port 8000
```
