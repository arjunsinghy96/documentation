# Writing GitMate Plugins

This tutorial contains step-by-step guide to create a GitMate Plugin with an example.

We'll be creating a plugin to acknowledge/unacknowledge commits in response to `ack` requests.
Let's name our plugin `ack`.

!!! note
    - Make sure you have set up the [development environment](./Development_Setup) before proceeding any furthur.
    - Replace every occurence of `ack` in this tutorial with your plugin name.

### 1. Initializing the plugin

From the project directory, run:
```bash
$ python manage.py startplugin ack
```

This will create a template plugin directory named `gitmate_ack` with following structure.

```
gitmate_ack
  |-- admin.py
  |-- apps.py
  |-- __init__.py
  |-- migrations/
  |     `-- ./
  |-- models.py
  |-- responder.py
  `-- tests/
        `-- ./
```

### 2. Registering the plugin

To register your plugin, open `gitmate/settings.py` and append `ack` to the list of strings called `GITMATE_PLUGINS`.
This adds your plugin to the list of recognized plugins. To check if everything is alright at this point 
run:

```bash
$ python manage.py runserver
```

You can run this command again in the future to check if everything is working.

### 3. Adding plugin settings

Plugin settings are the additional data your plugins might require to do their job. Our `ack` plugin requires two settings:

- `ack_strs`: Phrases that will be recognized as ack commands(comma seperated)
- `unack_strs`: Phrases that will be recognized as unack commands(comma seperated)

To add settings open `gitmate_ack/models.py` and add

```python
# Auto generated code
class Settings(models.Model):
    repo = models.OneToOneField(
        Repository, on_delete=models.CASCADE,
        related_name='gitmate_ack_repository')

    # Add your custom plugin settings here

    ack_strs = models.TextField(
        default='ack',
        help_text='Phrases that will be recognized as ack commands.')
    unack_strs = models.TextField(
        default='unack',
        help_text='Phrases that will be recognized as unack commands.')
```

### 4. Writing a responder

Plugins respond to distinctive events received through webhooks. Our plugin respond to pull request comments.
This means we have to attach our plugin to `IGitt.Interfaces.Actions.MergeActions.COMMENTED`. To do so, open 
`gitmate_ack/responders.py` and edit the `ResponderRegistrar.responder` decorator params. Then add your reponders code.

```python
@ResponderRegistrar.responder(
     'ack',
     MergeRequestActions.COMMENTED
 )
 def gitmate_ack(pr: MergeRequest,
                 comment: Comment,
                 ack_strs: str = 'ack',        # remember settings
                 unack_strs: str = 'unack'):   # make sure to add defaults
     """
     A responder to ack and unack commits
     """
     # This is responder logic for ack plugin
     # Add your own logic for your plugin

     body = comment.body.lower()
     commits = pr.commits
     pattern = '(^{k}\s)|(\s{k}\s)|(\s{k}$)'
 
     unack_strs = get_keywords(unack_strs)
     for kw in unack_strs:
         if re.search(pattern.format(k=kw), body):
             for commit in commits:
                 if commit.sha[:6] in body:
                     commit.unack()
 
     ack_strs = get_keywords(ack_strs)
     for kw in ack_strs:
         if re.search(pattern.format(k=kw), body):
             for commit in commits:
                 if commit.sha[:6] in body:
                     commit.ack()
```

Choose your responder event according to your needs. You could add multiple responders as well as multiple events to a responder. 
You can use any number of the plugin settings required by your responder. If default value is not provided, the settings won't 
be passed to the responder.

### 5. Adding a listener

Now you need to actually register the `MergeRequestAction.COMMENTED` listener with GitMate's webhook handler (otherwise it wouldn't
know what to listen for and your function in `responders.py` will never be called). To do this, go to `gitmate/views.py` and add
a listener. For example, for the `MergeRequestAction.COMMENTED` action we did:

```python
    if event == 'issue_comment':
         if webhook_data['action'] != 'deleted':
             comment = webhook_data['comment']
             pull_request_obj = GitHubMergeRequest(
                     token,
                     repository['full_name'],
                     webhook_data['issue']['number'])
             comment_obj = GitHubComment(
                     token,
                     repository['full_name'],
                     comment['id'])
             ResponderRegistrar.respond(
                     MergeRequestActions.COMMENTED,
                     repo_obj,
                     pull_request_obj,
                     comment_obj,
                     options=repo_obj.get_plugin_settings())
```

!!! note
    A PR comment is called `issue_comment` in GitHub's API - hence the `if event == 'issue_comment'`.

The body of webhook is available in `webhook_data` which contains additional information required by our responder.
The listener and the responder are connected via `ResponderRegistrar.respond`.

!!! note "A word about params"
    - First parameter is the event you registered your responder with. For `ack` plugin it is `MergeRequestActions.COMMENTED`.
    - Second parameter is `repo_obj` and it is mandatory.
    - The positional arguments for your responder follow next.
    - `options` param provide the keyword arguments(settings) to the responder.

### 6. Writing tests

Let's not forget to write tests for our newly created plugin. To add tests, go to `gitmate_ack/tests` and add a test file. Remember to use `GitmateTestClass` which makes it easy to write those test.

!!! success
    Our `ack` plugin is completed and fully functional.
