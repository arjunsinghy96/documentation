# Frequently Asked Questions (FAQ)

## Is the Community Edition just a Crippled Enterprise Edition?

NO. The GitMate community edition contains the core that allows us and you to write all possible features. We continuously develop new features for both versions. 
We are aware that a community edition without continuous development is just a useless dump of code. We strive to develop a "community" and if we don't deserve the community we're sure it will abandon us and make a fork.

## I can't afford the Enterprise Edition?

You can use the Community Edition, it contains awesome features and is actively developed.
However, if you are a non-profit or Open Source Project, you can contact us and we will make sure you can use the desired Enterprise Edition features.
