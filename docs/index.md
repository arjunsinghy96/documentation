# Welcome to GitMate 2

The new version of GitMate - written in django!

## What is GitMate?

GitMate automates the code review process on your projects hosted on Github(GitLab and BitBucket support coming soon) to speed up the development. GitMate provides static code analysis powered by coala and many more useful plugins specially tailored for your need.

Can't find a plugin you need? You can create you own!
